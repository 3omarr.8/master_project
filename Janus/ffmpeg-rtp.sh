ffmpeg -re -i TearsOfSteel.mp4 -y \
    -an -c:v copy \
    -f rtp rtp://127.0.0.1:8004 \
    -vn -acodec copy \
    -f rtp rtp://127.0.0.1:8006 > stream.sdp


ffmpeg -re -i TearsOfSteel.mp4 -y \
    -fflags +genpts \
    -an -c:v h264 \
    -f rtp rtp://127.0.0.1:8004
