const remoteVideo = document.getElementById("remoteVideo");
const button = document.getElementById("button");

var configuration = {
  offerToReceiveAudio: true,
  offerToReceiveVideo: true,
};

const pc = new RTCPeerConnection({
  configuration: configuration,
  iceServers: [],
});

pc.onnegotiationneeded = (event) => {};
pc.onconnectionstatechange = (event) => {};
pc.ontrack = (event) => {
  const remoteStream = event.streams[0];
  setVideoElement(remoteStream);
};
pc.onicecandidate = () => {};

button.onclick = async () => {
  let response = await fetch("http://localhost:3001/offer", {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({}),
  });
  let remoteOffer = await response.text();

  await pc.setRemoteDescription(
    new RTCSessionDescription({ sdp: remoteOffer, type: "offer" })
  );
  const answer = await pc.createAnswer();
  await pc.setLocalDescription(answer);

  await fetch("http://localhost:3001/answer", {
    method: "POST",
    headers: { "Content-Type": "text/plain" },
    body: answer.sdp,
  });
};

function setVideoElement(stream) {
  console.log(stream);
  if (!stream) return;
  remoteVideo.srcObject = stream;
}
